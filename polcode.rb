headings = [
    {id: 1, title: "heading1", heading_level: 0},
    {id: 2, title: "heading2", heading_level: 2},
    {id: 3, title: "heading3", heading_level: 1},
    {id: 4, title: "heading4", heading_level: 1}
]

headings2 = [
    {id: 1, title: "heading1", heading_level: 0},
    {id: 2, title: "heading2", heading_level: 3},
    {id: 3, title: "heading3", heading_level: 4},
    {id: 4, title: "heading4", heading_level: 1},
    {id: 5, title: "heading5", heading_level: 0},
    {id: 1, title: "heading1", heading_level: 0},
    {id: 2, title: "heading2", heading_level: 3},
    {id: 3, title: "heading3", heading_level: 4},
    {id: 4, title: "heading4", heading_level: 1},
    {id: 5, title: "heading5", heading_level: 0}
]

class Headings

  def initialize(data)
    @data = data
    @headings = []
    @labels = []
  end

  def call
    sorted = @data.sort_by {|x| x[:heading_level]}

    sorted.each_with_index do |heading, index|
      level = heading[:heading_level]

      number = Array.new(level + 1, 1)
      @labels << number
      prev = index - 1 >= 0 ? @labels[index-1] : nil

      num = get_label(prev, level, index)
      @labels[index] = num

      @headings << "#{num.join('.')} => #{heading[:title]}"
    end

    puts @headings
  end

  private

  def get_label(prev, level, index)
    return [1] if prev.nil?
    return prev if index.zero?

    new_number = Array.new(level + 1, 0)

    prev.each_with_index do |n, i|
      new_number[i] = prev[i]
    end

    new_number[level] = new_number[level] + 1

    new_number
  end
end

Headings.new(headings).call
Headings.new(headings2).call
